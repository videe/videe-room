import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors({
    origin: [
      'http://localhost:3000',
      'http://watch.videe.me',
      'http://watch.videe.me',
      'http://videe.me',
      'https://videe.me',
    ],
  });
  await app.listen(3001);
}
bootstrap();
