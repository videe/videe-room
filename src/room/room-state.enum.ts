export enum RoomStateEnum {
  playing = 1,
  paused = 2,
  buffering = 3,
}
