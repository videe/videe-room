import { Injectable } from '@nestjs/common';
import { animals, uniqueNamesGenerator } from 'unique-names-generator';
import { RoomStateEnum } from './room-state.enum';

@Injectable()
export class RoomService {
  private participants = [];
  private staleParticipants = [];
  private server: any;
  private state: RoomStateEnum = RoomStateEnum.paused;
  private startedAt: Date = null;

  public getParticipants() {
    return {
      active: this.participants.map((p) => {
        return { name: p.name };
      }),
      stale: this.staleParticipants.map((p) => {
        if (p.hasOwnProperty('name')) {
          return { name: p.name };
        } else {
          return p;
        }
      }),
    };
  }

  public participantUpdate(socketId, name) {
    const found = this.participants.find((p) => p.socketId === socketId);
    if (found) found.name = name;
  }

  public addParticipant(socketId, id) {
    if (!id) {
      return;
    }
    const foundStale = this.staleParticipants.find((p) => p.id === id);

    if (foundStale) {
      this.participants.push(foundStale);
      this.staleParticipants.splice(
        this.staleParticipants.indexOf(foundStale),
        1,
      );
    }

    const foundActive = this.participants.find((p) => p.id === id);

    if (foundActive) {
      foundActive.socketId = socketId;
      foundActive.timestamp = Date.now();
    } else {
      const participant = {
        id,
        socketId,
        name: uniqueNamesGenerator({
          dictionaries: [animals, animals],
          length: 2,
        }),
        timestamp: Date.now(),
      };
      this.participants.push(participant);
    }
  }

  public removeParticipant(socketId) {
    const participant = this.participants.find((p) => p.socketId === socketId);
    if (participant) {
      this.staleParticipants.push(participant);
      this.participants.splice(this.participants.indexOf(participant), 1);
    }
  }

  public cleanParticipants() {
    this.staleParticipants = [];
  }

  public setServer(server: any) {
    this.server = server;
  }

  public getServer() {
    return this.server;
  }

  public setState(roomState: RoomStateEnum) {
    this.state = roomState;
  }

  public getState() {
    return this.state;
  }

  public getRoomState() {
    return {
      state: this.state,
      startedAt: this.startedAt,
      participants: this.getParticipants(),
    };
  }
}
