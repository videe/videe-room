import {
  BaseWsExceptionFilter,
  OnGatewayConnection,
  OnGatewayDisconnect,
  OnGatewayInit,
  SubscribeMessage,
  WebSocketGateway,
} from '@nestjs/websockets';
import { UseFilters } from '@nestjs/common';
import { RoomService } from './room/room.service';
import { Cron, CronExpression } from '@nestjs/schedule';

@WebSocketGateway({
  cors: {
    origin: [
      'http://localhost:3000',
      'http://watch.videe.me',
      'http://watch.videe.me',
      'http://videe.me',
      'https://videe.me',
    ],
    credentials: true,
  },
  allowEIO3: true,
})
@UseFilters(new BaseWsExceptionFilter())
export class RoomGateway
  implements OnGatewayConnection, OnGatewayDisconnect, OnGatewayInit
{
  constructor(private roomService: RoomService) {}

  @Cron(CronExpression.EVERY_MINUTE)
  cleanParticipants() {
    this.roomService.cleanParticipants();
    this.roomService
      .getServer()
      .emit('roomState', this.roomService.getRoomState());
  }

  @SubscribeMessage('updateName')
  handleMessage(client: any, payload: any): void {
    this.roomService.participantUpdate(client.id, payload);
    this.roomService
      .getServer()
      .emit('roomState', this.roomService.getRoomState());
  }

  @SubscribeMessage('setState')
  handleUpdateState(client: any, payload: any) {
    this.roomService.setState(payload);
    this.roomService
      .getServer()
      .emit('roomState', this.roomService.getRoomState());
  }

  handleConnection(client: any): any {
    const id =
      client.handshake.query.id !== 'undefined'
        ? client.handshake.query.id
        : null;
    this.roomService.addParticipant(client.id, id);
    this.roomService
      .getServer()
      .emit('roomState', this.roomService.getRoomState());
  }

  handleDisconnect(client: any): any {
    this.roomService.removeParticipant(client.id);
    this.roomService
      .getServer()
      .emit('roomState', this.roomService.getRoomState());
  }

  afterInit(server: any): any {
    this.roomService.setServer(server);
  }
}
